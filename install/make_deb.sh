#!/bin/bash
#
# create a Debian package from the sources

# ------------------------------------------
# Config
# ------------------------------------------
SOURCE_DIR=src
SCRIPTS_DIR=debian/other
DEBIAN_META_DIR=debian/DEBIAN
DIST_DIR=dist
INTERNAL_PACKAGE_NAME=led
VERSION_FILE=./VERSION

REPO_PACKAGE_NAME=bmd-${INTERNAL_PACKAGE_NAME}

# installation dir of the replayer scripts suite
INSTALL_BASE_DIR=/opt/${INTERNAL_PACKAGE_NAME}
INSTALL_BIN_DIR=${INSTALL_BASE_DIR}

# path to the replayer config file
CONFIG_FILE=/etc/${INTERNAL_PACKAGE_NAME}.conf

# ------------------------------------------

set -e

# ---------------------------------------------------------------------------
# Read version file and remember the version number
#
# Globals:
#  none
# Arguments:
#   $1 ... version_file
# Sets:
#   version ... the version info
# Returns:
#  1 on error
# ---------------------------------------------------------------------------
function get_version {
    local version_file
    
    version_file=$1
    
    if [ ! "$version_file" ] ; then
        echo "[get_version] ERROR missing mandatory parameter version_file. Script error?"
        exit 1
    fi
    
    if [ ! -f "$version_file" ] ; then
        echo "[get_version] ERROR version file [$version_file] not found"
        exit 1
    fi
    
    version=$( cat "$version_file" | tr -d '\r' )
    # check for valid format
    if ! [[ "$version" =~ ^[0-9]+\.[0-9]+ ]] ; then
        echo "[get_version] ERROR unexpected value for version info, expected MAJOR.MINOR...etc., but found: [$version] in version file [$version_file]"
        exit 1
    fi
}


# ---------------------------------------------------------------------------
# Process cmd line arguments
#
# Globals:
#  none
# Arguments:
#   version
#   dist_dir
# Sets:
#   package_folder ... the name of the package folder, relative to the dist_dir
# Returns:
#  1 on error
# ---------------------------------------------------------------------------
function create_package_folder {
    local version dist_dir
    local pkg
    
    version=$1
    dist_dir=$2
    
    pkg="$dist_dir/${REPO_PACKAGE_NAME}-${version}"
    
    echo "creating package folder [$pkg] ..."
    
    if ! mkdir -p "$pkg" ; then
        echo "ERROR: unable to create package folder [$pkg]"
        exit 1
    fi
    
    package_folder=$pkg
}


# ---------------------------------------------------------------------------
# Copy script files to the Debian package
#
# Globals:
#  none
# Arguments:
#  "$@"
# Sets:
#   None
# Returns:
#  None
# ---------------------------------------------------------------------------
function copy_scripts {
    local scripts_dir package_folder
    local dest
    
    scripts_dir=$1
    package_folder=$2
    
    dest=$package_folder
    
    # copy all scripts into app directory
    echo "copying recursively script files to dest dir ..."
    if ! cp -r "$scripts_dir"/* "$dest" ; then
        echo "ERROR: unable to copy scripts from [$scripts_dir] to destination [$dest]"
        exit 1
    fi
    
    chmod -R 644 "$dest"
}


# ---------------------------------------------------------------------------
# Set version of Debian package
#
# Globals:
#  SOURCE_DIR
#  DEBIAN_META_DIR
# Arguments:
#  $1 ... package folder
#  $2 ... debian_source_dir
#  $3 ... package folder
#  $4 ... package name (for the Debian control file)
#  $5 ... version, e.g. "1.0-0"
# Sets:
#   None
# Returns:
#  None
# ---------------------------------------------------------------------------
function set_version {
    local package_folder
    local repo_package_name
    local source_folder
    local version
    local control_file
    local main_replayer_file
    
    source_folder=$1
    debian_source_dir=$2
    package_folder=$3
    repo_package_name=$4
    version=$5
    
    echo "setting version to [$version] ..."
    
    control_file=$debian_source_dir/control
    main_replayer_file=$source_folder/replayer
    
    if [ ! $repo_package_name"" ] ; then
        echo "ERROR [set_version] no package name passed to function. Script error?"
        exit 1
    fi
    if [ ! $version"" ] ; then
        echo "ERROR [set_version] no version passed to function. Script error?"
        exit 1
    fi
    
    if [ ! -f "$control_file" ] ; then
        echo "ERROR unable to find control file [$control_file]"
        exit 1
    fi
    
    sed -i "s/^Version: .*$/Version: $version/" "$control_file"
    sed -i "s/^Package: .*$/Package: $repo_package_name/" "$control_file"
    
    # also set version in main script, so that executing it with --version parameter
    # would display the correct version
    #sed -i "s/^version=.*$/version=${version}/" "$main_replayer_file"
}


# ---------------------------------------------------------------------------
# Copy sources to the Debian package
#
# Globals:
#  none
# Arguments:
#  "$@"
# Sets:
#   None
# Returns:
#  None
# ---------------------------------------------------------------------------
function copy_sources {
    local source_dir package_folder
    local dest
    
    source_dir=$1
    package_folder=$2
    
    echo "copying source files to package bin/ folder ..."
    
    dest="$package_folder/$INSTALL_BIN_DIR/"
    
    # copy all sources into app directory
    echo "creating dest dir ..."
    if ! mkdir -p "$dest" ; then
        echo "ERROR: unable to create destination dir [$dest]"
        exit 1
    fi
    
    echo "copying source files to dest dir ..."
    if ! cp -r "$source_dir/"/* "$dest" ; then
        echo "ERROR: unable to copy sources from [$source_dir] to destination [$dest]"
        exit 1
    fi
    
    # remove __pycache__ directories
    set +e
    if ! find "$dest" -type d -name '__pycache__' -exec rm -fr {} \; ; then
        : # never mind if we found nothing
    fi
    set -e
    
    chmod -R 755 "$dest"
}


# ---------------------------------------------------------------------------
# Copy metadata files into Debian package
#
# Globals:
#  none
# Arguments:
#  "$@"
# Sets:
#   None
# Returns:
#  None
# ---------------------------------------------------------------------------
function copy_debian_metadata {
    local debian_source_dir package_folder
    local dest
    
    debian_source_dir=$1
    package_folder=$2
    
    dest=$package_folder/DEBIAN
    
    # copy all metadata files into DEBIAN directory
    echo "copying Debian metadata files to dest dir ..."
    if ! mkdir -p "$dest" ; then
        echo "ERROR: unable to create DEBIAN directory [$dest]"
        exit 1
    fi
    if ! cp "$debian_source_dir/"* "$dest" ; then
        echo "ERROR: unable to copy metadata files to destination [$dest]"
        exit 1
    fi
    
    chmod 755 "$dest"
    [ -e "$dest/postinst" ] && chmod 755 "$dest/postinst"
    [ -e "$dest/preinst" ] && chmod 755 "$dest/preinst"
    [ -e "$dest/postrm" ] && chmod 755 "$dest/postrm"
    [ -e "$dest/prerm" ] && chmod 755 "$dest/prerm"
}



# ---------------------------------------------------------------------------
# Build the Debian package
#
# Globals:
#  none
# Arguments:
#  "$@"
# Sets:
#   None
# Returns:
#  None
# ---------------------------------------------------------------------------
function build_package {
    local dist_dir package_folder
    local ok=1
    
    dist_dir=$1
    package_folder=$2
    
    (
        cd "$dist_dir"
        if ! dpkg-deb --build "$package_folder" ; then
            ok=0
        fi
    )
    if [ "$ok" -ne 1 ] ; then
        echo "ERROR at building Debian package"
        exit 1
    fi
}

# ---------------------------------------------------------------------------
# Convert all line endings to Unix style
#
# Globals:
#  none
# Arguments:
#  "$@"
# Sets:
#   None
# Returns:
#  None
# ---------------------------------------------------------------------------
function unix_file_endings {
    local package_folder
    local ok=1
    
    package_folder=$1
    
    echo "changing line endings to Unix style ..."
    
    if ! find "$package_folder" -type f -exec dos2unix {} \; ; then
        echo "ERROR at changing line ending to <LF>"
        exit 1
    fi
}

# ---------------------------------------------------------------------------
# Modify central replayer script: fill in path to the scripts
#
# Globals:
#  none
# Arguments:
#  $1 ... package folder
# Sets:
#   None
# Returns:
#  None
# ---------------------------------------------------------------------------
function set_bin_dir {
    local package_folder
    local install_bin_dir
    local service_file
    
    package_folder=$1
    install_bin_dir=$2
    
    postinst_file=${package_folder}/DEBIAN/postinst
    postrm_file=${package_folder}/DEBIAN/postrm
    
    local dollar='$'
    
    service_file="${package_folder}/lib/systemd/system/led.service"
    
    echo "setting bin dir to [$install_bin_dir] in the LED service file ..."
    sed -i "s|^ExecStart=/.*$dollar|ExecStart=$install_bin_dir/app.py|" "$service_file"
    
    echo "setting bin dir to [$install_bin_dir] in the postinst file ..."
    sed -i "s|^bin_dir=.*$dollar|bin_dir=$install_bin_dir|" "$postinst_file"
    
    echo "setting bin dir to [$install_bin_dir] in the postrm file ..."
    sed -i "s|^bin_dir=.*$dollar|bin_dir=$install_bin_dir|" "$postrm_file"
}


# ---------------------------------------------------------------------------
# Generate word completion file containing all possible commands
#
# Globals:
#  none
# Arguments:
#  $1 ... package folder
# Sets:
#   None
# Returns:
#  None
# ---------------------------------------------------------------------------
function generate_word_completion_commands {
    local source_dir
    local scripts_dir
    local main_replayer_file
    local word_completion_file
    local commands
    
    source_dir=$1
    scripts_dir=$2
    
    word_completion_file=${scripts_dir}/etc/bash_completion.d/${INTERNAL_PACKAGE_NAME}
    main_replayer_file=${source_dir}/replayer
    
    if [ ! -f "$word_completion_file" ] ; then
        echo "INFO could not find replayer word completion file at [$word_completion_file], skipping step"
        exit 1
    fi
    
    chmod +x "$main_replayer_file" # necessary under Windows? Linux should set the executable flag for git correctly ...
    commands=$( "$main_replayer_file" --commands )
    if [ ! "$commands" ] ; then
        echo "ERROR unable to retrieve command list from replayer script"
        exit 1
    fi
    
    echo "setting list of possible commands in word completion file [$word_completion_file] ..."
    local dollar='$'
    sed -i "s|^commands=.*$dollar|commands='$commands'|" "$word_completion_file"
    
    chmod 644 "$word_completion_file"
}

# ---------------------------------------------------------------------------
# Generate word completion file containing all possible commands
#
# Globals:
#  none
# Arguments:
#  $1 ... package folder
# Sets:
#   None
# Returns:
#  None
# ---------------------------------------------------------------------------
function set_dirs_executable_flag {
    local base_dir
    
    base_dir=$1
    
    if [ ! "$base_dir" ] ; then
        echo "[set_dirs_executable_flag] ERROR missing mandatory parameter 'base_dir'"
        exit 1
    fi
    
    find "$base_dir" -type d -exec chmod +x {} \;
}

# ---------------------------------------------------------------------------
# Check that the current user is root (otherwise the files will be owned by
# the current user - we cannot be sure which user this is, so ...)
#
# Globals:
#  none
# Arguments:
#  none
# Sets:
#   None
# Returns:
#  0 if user is root, an other value (the UID of the current user) if not
# ---------------------------------------------------------------------------
function check_root_user {
    return $( id -u )
}

# ---------------------------------------------------------------------------
# MAIN
#
# Globals:
#  none
# Arguments:
#  "$@"
# Sets:
#  version
# Returns:
#  None
# ---------------------------------------------------------------------------
function process_cmd_line_args {
    
    version=$1
    
    if [ ! "$version" ] ; then
        echo "ERROR missing mandatory parameter VERSION"
        exit 1
    fi
    echo "Version is [$version]"
}


# ---------------------------------------------------------------------------
# MAIN
#
# Globals:
#  none
# Arguments:
#  "$@"
# Sets:
#  None
# Returns:
#  None
# ---------------------------------------------------------------------------
function main {
    local script_dir base_dir dist_dir source_dir
    
    # check if current user is root
    if ! check_root_user ; then
        echo "You must be root to run this script"
        exit 1
    fi
    
    # fetch version from version file, not from command line args
    #process_cmd_line_args "$@" # version
    
    script_dir=$( dirname "$0" )
    base_dir=$( realpath $script_dir/.. )
    dist_dir=$( realpath $base_dir/dist )
    source_dir=$( realpath $base_dir/$SOURCE_DIR )
    scripts_dir=$( realpath $base_dir/$SCRIPTS_DIR )
    debian_source_dir=$( realpath $base_dir/$DEBIAN_META_DIR )
    
    if [ ! -d "$dist_dir" ] ; then
        echo "WARNING: could not find dist dir [$dist_dir]. Creating it ... (wait 2 seconds)"
        sleep 2
        mkdir "$dist_dir"
    fi
    
    
    get_version "$VERSION_FILE" # version
    
    
    # make new package folder
    create_package_folder "$version" "$dist_dir" # package_folder
    
    set_version "$source_dir" "$debian_source_dir" "$package_folder" "$REPO_PACKAGE_NAME" "$version"
    
    #generate_word_completion_commands "$source_dir" "$scripts_dir" # TODO optional setp?
    
    copy_sources "$source_dir" "$package_folder"
    
    copy_scripts "$scripts_dir" "$package_folder"
    
    copy_debian_metadata "$debian_source_dir" "$package_folder"
    
    unix_file_endings "$package_folder"
    
    set_dirs_executable_flag "$package_folder"
    
    set_bin_dir "$package_folder" "$INSTALL_BIN_DIR"
    
    build_package "$dist_dir" "$package_folder" # debian_package_filename
    
    echo "Built Debian package."
    echo "Finished."
}

main "$@"