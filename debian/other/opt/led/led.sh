#!/bin/bash
#
# small wrapper for the Pi LED service
#
# Usage:
#    $0 red
#    $0 red/blink
#    $0 green
#    $0 green/blink
#    $0 yellow
#    $0 yellow/blink
#    $0 off
#    $0 pattern R.r.y.g_G_
#    $0 pattern R.r.y.g_G_ 3s|2m
#    $0 reset
#    $0 status
#    $0 help

# -----------------------------
# CONFIG
# -----------------------------

# URL of Pi LED service
SERVICE=http://localhost:8000

# -----------------------------
# CONFIG END
# -----------------------------


# ---------------------------------------------------------------------------
# Request the LED service
#
# Globals:
#  SERVICE ... base URL of LED service REST API
# Arguments:
#  $1 ... command
#  $2 ... timeout (if any)
# Returns:
#  None
# ---------------------------------------------------------------------------
function request {
    local cmd
    local timeout
    
    cmd=$1
    timeout=$2
    
    if [ "$timeout" ] ; then
        timeout_param="?timeout=$timeout"
    fi
    
    if [[ "$cmd" =~ ^[rRgGyY._]+$ ]] ; then
        # apparently the command is a LED pattern
        curl "$SERVICE/pattern/$cmd$timeout_param"
        elif [ "$cmd" == 'quit' ] ; then
        echo "command 'quit' is not allowed with this wrapper script"
    else
        curl "$SERVICE/$cmd$timeout_param"
    fi
}

# ---------------------------------------------------------------------------
# Print usage screen
#
# Globals:
#  none
# Arguments:
#  "$@"
# Returns:
#  None
# ---------------------------------------------------------------------------
function usage {
    echo "Usage:"
    echo "  $( basename $0 ) [options] [ COMMAND ]"
    echo
    echo "Options:"
    echo "  -t, --time ...use this new LED setting only for a timespan. The timespan can be specified"
    echo "             as e.g. '10s', '2m'. Only 's' (seconds) and 'm' (minutes) are allowed. After "
    echo "             this time, the former LED status (or pattern) will be restored."
    echo "  -h, --help ... print this usage screen"
    echo
    echo "Parameters:"
    echo "  COMMAND ... can be one of:"
    echo "           red, green, yellow, red/blink, green/blink, yellow/blink"
    echo "           off ... turn LED off"
    echo "           status ... get LED status"
    echo "           <any other> ... will be interpreted as pattern, e.g. 'r.g.'"
}


# ---------------------------------------------------------------------------
# Process cmd line arguments
#
# Globals:
#  none
# Arguments:
#  "$@"
# Sets:
#   run_id
# Returns:
#  1 on error
# ---------------------------------------------------------------------------
function process_cmd_args {
    
    options=$( getopt -o ht: --long help,time: -n 'parse-options' -- "$@" )
    
    if [ $? != 0 ] ; then
        error "Failed parsing command line arguments." >&2
        exit 1
    fi
    
    eval set -- "$options"
    while true; do
        case "$1" in
            -t | --time ) timeout=$2; shift; shift ;;
            -h | --help ) usage; exit 0 ;;
            -- ) shift; break ;;
            * ) break ;;
        esac
    done
    
    cmd=$1
    
    if [ ! "$cmd" ] ; then
        echo "ERROR: missing mandatory parameter COMMAND. Use -h/--help for usage screen."
        exit 1
    fi
}


# ---------------------------------------------------------------------------
# Main
#
# Globals:
#  run_id
#  pi_login
#  ACTIVE_STREAMER
# Arguments:
#  $1 run id
# Returns:
#  1 on error
# ---------------------------------------------------------------------------
function main {
    local cmd
    local timeout
    
    process_cmd_args "$@"
    
    request "$cmd" "$timeout"
}


main "$@"