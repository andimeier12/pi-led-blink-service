# PI LED service

Service (daemon) for controlling the LEDs on the GPIO pin(s) of the Raspberry.

Intended to be used in the Replayer environment, where the LEDs of the PI (trainserver) might signal something.

It basically consists of a simple HTTP server which can be used to trigger the possible LED states.

## Usage

### Start server

The server is started with

    py main.py

and runs forever.

Note that for accessing the GPIO pins you must be superuser or member of the group `gpio`.
Thus, the server must started with an appropriate user.

### Stop server

You can use `CTRL-C` to stop the server if started in an interactive shell.

A second option is:

    GET /quit

This special route will also terminate the server.

## Hardware setup

On 2 GPIO pins, the 2 input pins from a Duo LED are connected. In other words: the controlled LED can be:

- red
- green
- yellow (= both red and green pin switched on)

## HTTP routes

The following HTTP routes can be used:

- `GET /red` ... switch on red LED
- `GET /green` ... switch on green LED
- `GET /yellow` ... switch on yellow LED
- `GET /blink` ... keep last color, but switch on blinking mode
- `GET /blink/red` ... switch on red blinking mode
- `GET /blink/green` ... switch on green blinking mode
- `GET /blink/yellow` ... switch on yellow blinking mode
- `GET /pattern?sequence=SEQUENCE` ... run through a custom sequence (s. section "Custom pattern" below)
- `GET /off` ... turn off LED altogether
- `GET /status` ... print current LED status
- `GET /help` ... print help page
- `GET /quit` ... quit LED service (LED will be switched off on exit)

If you add a query parameter `time` on a request which sets or clears the LED somehow,
then the requested LED pattern will only be active for the specified amount of time.
After that, the previous pattern will be restored.
This can be used to temporarily let the LED flash wildly (for indicating the right position on a front panel).

The `time` parameter is made like this:

- `10s` ... 10 seconds
- `2m` ... 2 minutes

Only "seconds" and "minutes" are supported, since this function is intended to set a _temporary_ pattern.

Each command returns with the response of `GET /status`. The response is plain text and describes the state of the LED
in human readable but compact form, e.g.:

    RED

or:

    GREEN (blinking)

or:

    Pattern: R_G_Y_

## Custom pattern

You can control the LEDs via a custom blink pattern:

    GET /pattern?sequence=SEQUENCE

... where SEQUENCE is a string made up of the following characters in any number and order:

- `R` ... long red interval
- `r` ... short red interval
- `G` ... long green interval
- `g` ... short green interval
- `Y` ... long yellow interval
- `y` ... short yellow interval
- `_` ... long "dark" interval (no LED switched on)
- `.` ... short "dark" interval (no LED switched on)

Example:

    GET /pattern?sequence=r.

This lets the red LED blink with a high frequency. It is exactly the same as:

    GET /red/blink

Another example:

    GET /pattern?sequence=R.y.y.G___Y.

... you get the point, don't you?

The custom pattern will be looped endlessly, so in most cases it should start with some "switch LED on" command and end with some "switch LED off" command.

## Configuration

The service is configured via a TOML config script which is located in the same directory as the main script. It is called:

    config.toml

By using this, you can set the listening port, log level etc.

## Necessary Python modules

See `requirements.txt` for a list of necessary installed modules.

## Simulation mode (without hardware)

The service features an LED simulation mode which can be used to check the functions without actually running on a Pi.
The config setting `ledDriver` determines whether the LED simulation will be used or the "real" GPIO pins.

With the following setting the LED simulation mode will be used:

    ledDriver = "simulated"

In this case, you should also set the log level to `debug` since the simulated LED states are output via the debug log channel (otherwise you would not see anything):

    logLevel = "debug"

## Wrapper script

On the Raspberry Pi, typically a wrapper script will be used, so you don't have to deal with HTTP request.
Instead, the command "led" has the following interface:

    led red # switch on red light
    led red/blink # switch on red blinking light
    led pattern r.g.y. # switch on pattern blinking
    led off # switch off light
    led --help # print help page

This wrapper script should be located in /usr/local/binm so it can be used on the shell without a
path.

## Test installation using Docker

```
docker build -t led .
docker run -it led /bin/bash
```

## Update to next version

The versioning is supported via `bumpversion`. Install

    pip install --upgrade bump2version

so that you have the tool at hand.

Bump version examples:

    bumpversion patch ; cat VERSION

## Build Debian package

Run the following script:

    fakeroot git/pi ./make_deb.sh

It will read the VERSION file and then build the Debian package.
