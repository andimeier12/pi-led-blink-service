from distutils.core import setup

setup(
    name='led',
    version='1.1.0',
    packages=['led', ],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.txt').read(),
)
