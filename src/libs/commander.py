from .ledSettings import ledSettings


class Commander:

    def red(self, params):
        self._set_pattern('R', params)

    def green(self, params):
        self._set_pattern('G', params)

    def yellow(self, params):
        self._set_pattern('Y', params)

    def off(self, params):
        self._set_pattern('_', params)

    def pattern(self, params):
        self._set_pattern(params['pattern'], params)

    def cancel_overdub(self, params):
        ledSettings.end_overdub()

    def status(self, params=None):
        return ledSettings.status()

    def stop(self, params):
        # stop worker
        pass

    def _set_pattern(self, pattern, params):
        if 'blink' in params:
            pattern = pattern + '_'
            
        if 'timeout' in params:
            timeout = params['timeout']
        else:
            timeout = None
            
        if timeout is not None:
            ledSettings.overdub(pattern, timeout)
        else:
            ledSettings.set_pattern(pattern)


commander = Commander()
