from .log import logger
from colorama import Fore, Back, Style, init

init()


class LedDriver:
    """
    the physical layer of the LEDs. This is the class which actually sets the right register bits in order
    to achieve the desired LED color.
    This class in particular is a software simulation of a hardware LED. It simply prints
    the simulated LED state via debug log statements
    """

    def __init__(self):
        self._redOn = False
        self._greenOn = False
        self._printLeds()

    def __del__(self):
        # switch off LED on exit
        logger.debug('switching LED off on exit')
        self.clear()

    def lightRed(self):
        if not self._redOn:
            # turn on red LED
            self._redOn = True
            logger.debug("RED turned on")
        if self._greenOn:
            self._greenOn = False
            logger.debug("green turned off")
        self._printLeds()

    def lightGreen(self):
        if not self._greenOn:
            # turn on green LED
            self._greenOn = True
            logger.debug("GREEN turned on")
        if self._redOn:
            self._redOn = False
            logger.debug("red turned off")
        self._printLeds()

    def lightYellow(self):
        if not self._redOn:
            # turn on yellow LED
            self._redOn = True
            logger.debug("RED turned on")
        if not self._greenOn:
            self._greenOn = True
            logger.debug("GREEN turned on")
        self._printLeds()

    def clear(self):
        if self._redOn:
            # turn off red LED
            self._redOn = False
            logger.debug("red turned off")
        if self._greenOn:
            self._greenOn = False
            logger.debug("green turned off")
        self._printLeds()

    def printLeds(self):
        pass

    def _printLeds(self):
        #def pos(y, x): return '\x1b[%d;%dH' % (y, x)
        def pos(y, x): return ''

        if (self._redOn and self._greenOn):
            text = Back.YELLOW + ' Y ' + Style.RESET_ALL
        elif (self._redOn and not self._greenOn):
            text = Back.RED + ' R ' + Style.RESET_ALL
        elif (not self._redOn and self._greenOn):
            text = Back.GREEN + ' G ' + Style.RESET_ALL
        else:
            text = '.'
        print("%s%s" % (pos(1, 1), text))
