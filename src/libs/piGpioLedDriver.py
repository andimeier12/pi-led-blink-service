from gpiozero import LED
from .log import logger
from .config import config


class LedDriver:
    """
    the physical layer of the LEDs. This is the class which actually sets the right register bits in order
    to achieve the desired LED color.
    This class in particular is a software simulation of a hardware LED. It simply prints
    the simulated LED state via debug log statements
    """

    def __init__(self):
        self._redOn = False
        self._greenOn = False
        self.__initGpio()
        self._red(False)
        self._green(False)

    def __initGpio(self):
        # setup used pins as output
        self._redLed = LED(config['gpio']['gpioPinRed'])
        self._greenLed = LED(config['gpio']['gpioPinGreen'])

    def _red(self, state):
        if (state):
            self._redLed.on()
        else:
            self._redLed.off()
        self._redOn = state

    def _green(self, state):
        if (state):
            self._greenLed.on()
        else:
            self._greenLed.off()
        self._greenOn = state

    def lightRed(self):
        if not self._redOn:
            # turn on red LED
            self._red(True)
            logger.debug("RED turned on")
        if self._greenOn:
            self._green(False)
            logger.debug("green turned off")

    def lightGreen(self):
        if not self._greenOn:
            # turn on green LED
            self._green(True)
            logger.debug("GREEN turned on")
        if self._redOn:
            self._red(False)
            logger.debug("red turned off")

    def lightYellow(self):
        if not self._redOn:
            # turn on yellow LED
            self._red(True)
            logger.debug("RED turned on")
        if not self._greenOn:
            self._green(True)
            logger.debug("GREEN turned on")

    def clear(self):
        if self._redOn:
            # turn off red LED
            self._red(False)
            logger.debug("red turned off")
        if self._greenOn:
            self._green(False)
            logger.debug("green turned off")

    def printLeds(self):
        pass
