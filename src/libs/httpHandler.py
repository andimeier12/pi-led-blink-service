from http.server import BaseHTTPRequestHandler
from uritools import urisplit
import sys
import threading
import os
from .log import logger
from .ledSettings import LedSettings
from .commander import commander


class Server(BaseHTTPRequestHandler):

    def log_request(self, code='-', size='-'):
        if logger.isLogged('info'):
            # log requests with log level 'info'
            super().log_request(code, size)

    def do_HEAD(self):
        return

    def do_GET(self):
        try:
            self.respond()
        except Exception as e:
            response = self.handle_http(400, 'text/plain', e)
            self.wfile.write(response)


    def do_POST(self):
        return

    def handle_http(self, status, content_type, content):
        self.send_response(status)
        self.send_header('Content-type', content_type)
        self.end_headers()
        return bytes(content, "UTF-8")

    def signalInt(self):
        # signal async worker to stop
        commander.stop()

    def respond(self):
        quit = False

        # router table
        uriparts = urisplit(self.path)
        path = uriparts.path
        query = uriparts.getquerydict()
        params = {}

        if (path == '/help'):
            handler = help

        elif (path == '/red'):
            handler = commander.red

        elif (path == '/green'):
            handler = commander.green

        elif (path == '/yellow'):
            handler = commander.yellow

        elif (path == '/red/blink'):
            handler = commander.red
            params['blink'] = True

        elif (path == '/green/blink'):
            handler = commander.green
            params['blink'] = True

        elif (path == '/yellow/blink'):
            handler = commander.yellow
            params['blink'] = True

        elif (path[:9] == '/pattern/'):
            pattern = path[9:]
            params['pattern'] = pattern
            handler = commander.pattern

        elif (self.path == '/status'):
            handler = commander.status

        elif (self.path == '/off'):
            handler = commander.off

        elif (self.path == '/reset'):
            handler = commander.cancel_overdub

        elif (self.path == '/quit'):
            handler = (lambda x: "Server terminated via HTTP request.")
            quit = True

        else:
            self.send_error(400, "Unknown route: " + self.path)
            return

        timeout = query.get('timeout')
        if timeout is not None:
            # query variable can occur multiple times, that's why query.get returns 
            # an array. We need the first occurrence
            params['timeout'] = timeout[0]

        try:
            content = handler(params)
        except Exception as e:
            self.send_error(400, "Error unable to parse request: " + self.path + ', due to: ' + e)
            return

        if content is None:
            content = commander.status()
        response = self.handle_http(200, 'text/plain', content)
        self.wfile.write(response)

        if quit:
            print("Server terminated via HTTP request.")
            # use another "assassin thread" to tell the server to quit
            # Calling shutdown directly in this request handler would not work
            # because shutdown waits for the handler to finish, the shuts down
            # server - and the handler on his part will wait for shutdown()
            # to return - a concurrent locking situation. So, employ as assassin ...
            threading.Thread(target=self.server.shutdown, daemon=True).start()


def help(params):
    script_dir = os.path.realpath(os.path.join(
        os.getcwd(), os.path.dirname(__file__)))
    f = open(os.path.join(script_dir, "helptext.txt"), "r")
    if f.mode == 'r':
        content = f.read()
        return content
    else:
        return "ERROR: Could not read help text"
