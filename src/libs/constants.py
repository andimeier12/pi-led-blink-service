from enum import Enum, unique


@unique
class Color(Enum):
    RED = 1
    GREEN = 2
    YELLOW = 3


@unique
class Lit(Enum):
    OFF = 1
    ON = 2


@unique
class Mode(Enum):
    PERMANENT = 1
    BLINKING = 2
    PATTERN = 3
