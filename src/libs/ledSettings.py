import threading
import time


class LedSettings:

    def __init__(self):
        # lock for securing data exchange between controlling app and asynchronous LED worker thread
        self.lock = threading.RLock()
        self.pattern = ' '  # default pattern: off
        self.pattern_index = 0
        self.pattern_length = 1
        self.underlying_pattern = None
        self.overdub_pattern = None
        self.overdub_until = None  # a value of not None indicates active overdubbing

    def set_pattern(self, pattern):
        with self.lock:
            self.pattern = pattern
            self.pattern_length = len(pattern)
            self.pattern_index = 0
            # cancel any overdubbing, if any
            self.overdub_until = None

    def overdub(self, pattern, timeout):
        with self.lock:
            self.overdub_pattern = pattern
            self.underlying_pattern = self.pattern
            self.pattern = pattern

            # start (or re-start) the timer
            wait_seconds = self._calculate_timespan(
                timeout)
            self.overdub_until = time.time() + float(wait_seconds)

            # start from the beginning of the overdub pattern
            self.pattern_length = len(pattern)
            self.pattern_index = 0

    def end_overdub(self):
        with self.lock:
            self._end_overdub()

    def _end_overdub(self):
        self.overdub_until = None
        self.pattern = self.underlying_pattern

        # start from the beginning of the overdub pattern
        self.pattern_length = len(self.pattern)
        self.pattern_index = 0

    def get_pattern(self):
        with self.lock:
            pattern = self.pattern
        return pattern

    def next_instruction(self):
        with self.lock:
            if self.overdub_until is not None:
                if time.time() > self.overdub_until:
                    # switch back from overdubbing to underlying pattern
                    self._end_overdub()

            instruction = self.pattern[self.pattern_index]
            self.pattern_index += 1
            if self.pattern_index >= self.pattern_length:
                self.pattern_index = 0  # wrap around

        return instruction

    def status(self):
        with self.lock:
            current_pattern = self.pattern
            overdub_until = self.overdub_until
            underlying_pattern = self.underlying_pattern

        status = self._build_status_text(
            current_pattern, overdub_until is not None, underlying_pattern, overdub_until)

        return 'status: [' + status + "]\n"

    def _build_status_text(self, current_pattern, overdubbing, underlying_pattern, overdub_until):
        status = current_pattern
        if overdubbing:
            # status = status + ' (overdubbing until ' + \
            #     str(self._time_left(oberdub_until)) + ') / ' + underlying_pattern
            status = status + ' (overdubbing until ' + \
                str(overdub_until) + ') / ' + underlying_pattern

        return status

    def _time_left(self, until):
        return int(until - time.time())

    def _calculate_timespan(self, timeout):
        unit = timeout[-1]
        try:
            number = int(timeout[:-1])
        except ValueError:
            raise ValueError(
                'illegal time format, must be something like "12s" or "5m", but was: ' + timeout)

        if unit == 's':
            seconds = int(number)
        elif unit == 'm':
            seconds = int(number * 60)
        else:
            raise ValueError(
                'illegal time format, only s (seconds) and m (minutes) are allowed, but it was: ' + timeout)

        if seconds < 1:
            raise ValueError(
                "time value must be 1 or greater, but is: " + str(seconds))

        return seconds


ledSettings = LedSettings()
