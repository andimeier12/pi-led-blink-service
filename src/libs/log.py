logLevels = {
    'debug': 1,
    'info': 2,
    'warn': 3,
    'error': 4
}


class Logger:

    def __init__(self):
        # if no logLevel is set, use "error" as default
        self.setLogLevel('error')

    @staticmethod
    def getLogLevelIndex(logLevelName):
        """returns the numerical representation of the given log level

        If no log level with this name is found, return 0
        """
        return logLevels.get(logLevelName, 0)

    def setLogLevel(self, logLevelName: str):
        """sets a new log level

        Arguments:
        logLevel ... the new log level, e.g. 'debug'
        """
        logLevel = Logger.getLogLevelIndex(logLevelName)
        if logLevel:
            self.logLevel = logLevel
            self.logLevelName = logLevelName
        else:
            raise ValueError(
                "illegal value for logLevel [" + logLevelName + "]")

    def getLogLevel(self):
        return self.logLevelName

    def isLogged(self, logLevelName):
        """determines whether the given log level would be logged or not"""
        logLevel = Logger.getLogLevelIndex(logLevelName)
        return logLevel >= self.logLevel

    def log(self, level, message):
        if (logLevels.get(level, 0) >= self.logLevel):
            print('[LOG] ' + str(message))

    def debug(self, message):
        """convenience function, same as log('debug', ...)"""
        if (logLevels.get('debug', 0) >= self.logLevel):
            print('[DEBUG] ' + str(message))

    def info(self, message):
        """convenience function, same as log('info', ...)"""
        if (logLevels.get('info', 0) >= self.logLevel):
            print('[INFO] ' + str(message))

    def warn(self, message):
        """convenience function, same as log('warn', ...)"""
        if (logLevels.get('warn', 0) >= self.logLevel):
            print('[WARN] ' + str(message))

    def error(self, message):
        """convenience function, same as log('error', ...)"""
        if (logLevels.get('error', 0) >= self.logLevel):
            print('[ERROR] ' +str( message))


logger = Logger()
