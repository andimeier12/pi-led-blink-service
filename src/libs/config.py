import toml
import sys
import os

config = {}

config_dir = '/etc/opt/led'


def load():
    configFile = os.path.join(config_dir, 'led.conf.toml')
    try:
        global config
        config = toml.load(configFile)
        print("LOADED CONFIG FILE")

    except:
        print("could not open config file [" + configFile + "]")
        sys.exit(2)

    # make sure the configured durations are valid numbers
    for duration in ['long', 'short']:
        print("checking configured duration [" + duration + "Step] ...")
        try:
            val = float(config['led'][duration + 'Step'])
        except ValueError:
            print("Error: config setting [led][" +
                  duration + "Step] must be numeric")
            sys.exit(2)
        # check for a positive, non-zero value
        if val <= 0 or val > 10:
            print("Error: config setting [led][" +
                  duration + "Step] must be in the range <0;10]")
            sys.exit(2)


load()
