import threading
import time
import datetime
import sys
from .constants import Color, Lit, Mode
from .log import logger
from .config import config
from .ledSettings import ledSettings

# duration of a "long step" in seconds
STEP_LONG = config['led']['longStep']

# duration of a "short step" in seconds
STEP_SHORT = config['led']['shortStep']


class Runner():
    """
    asynchronous LED worker class. Generates patterns and uses the physical layer as device
    """

    def __init__(self):
        self.stopWorker = False

        # load the desired LED driver module
        ledDriverModule = config['ledDriver']
        if ledDriverModule == 'simulated':
            print("SIMULATED LED DRIVER")
            from .simulatedLedDriver import LedDriver
        elif ledDriverModule == 'gpio':
            print("GPIO LED DRIVER")
            from .piGpioLedDriver import LedDriver
        else:
            raise ValueError(
                "illegal value for config setting 'ledDriver': " + ledDriverModule)

        self.ledDriver = LedDriver()
        self.worker = threading.Thread(
            target=self.run, args=(ledSettings, lambda: self.stopWorker,))

    def __del__(self):
        self.stop()
        
    def start(self):
        self.worker.start()

    def stop(self):
        self.stopWorker = True

    def run(self, ledSettings, stop):

        while True:
            #logger.debug(datetime.datetime.now())

            # in an endless loop, process character after character of the pattern string
            # Each character is a "pattern command", e.g. 'R' means "switch red on for a long interval"
            # 'y' means "switch yellow light on for a short interval"
            instruction = ledSettings.next_instruction()

            if instruction == 'r':
                self.ledDriver.lightRed()
                time.sleep(STEP_SHORT)
            elif instruction == 'g':
                self.ledDriver.lightGreen()
                time.sleep(STEP_SHORT)
            elif instruction == 'y':
                self.ledDriver.lightYellow()
                time.sleep(STEP_SHORT)
            if instruction == 'R':
                self.ledDriver.lightRed()
                time.sleep(STEP_LONG)
            elif instruction == 'G':
                self.ledDriver.lightGreen()
                time.sleep(STEP_LONG)
            elif instruction == 'Y':
                self.ledDriver.lightYellow()
                time.sleep(STEP_LONG)
            elif instruction == '.':
                self.ledDriver.clear()
                time.sleep(STEP_SHORT)
            elif instruction == '_':
                self.ledDriver.clear()
                time.sleep(STEP_LONG)

            if stop():
                logger.debug("Cleaning up ...")
                # turn LED off on exit
                self.ledDriver.clear()
                break

        print("Worker stopped.")


# globally setup LED object once
ledRunner = Runner()
