# !/usr/bin/env python3
import time
import sys
import os
from http.server import HTTPServer
from libs.config import config
from libs.httpHandler import Server
from libs.commander import commander
from libs.log import logger
from libs.ledState import ledRunner

def run():
    # ---------------------------------
    # sanity checking config parameters
    # ---------------------------------

    if not config['logLevel']:
        print("ERROR missing config parameter [logLevel]")
        sys.exit(1)
    if not config['serverPort']:
        print("ERROR missing config parameter [serverPort]")
        sys.exit(1)
    if not config['serverHostname']:
        print("ERROR missing config parameter [serverHostname]")
        sys.exit(1)

    # -------------
    # set log level
    # -------------
    logger.setLogLevel(config['logLevel'])

    # start LED
    ledRunner.start()

    httpd = HTTPServer(
        (config['serverHostname'], config['serverPort']), Server)
    print(time.asctime(), 'Server UP - listening on %s:%s' %
          (config['serverHostname'], config['serverPort']))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print("CTRL-C detected ... shutting down ...")
        ledRunner.stop()
        pass
    httpd.server_close()
    print(time.asctime(), 'Server DOWN - stop listening on %s:%s' %
        (config['serverHostname'], config['serverPort']))

# ------------
# start server
# ------------

if __name__ == '__main__':
    run()
