# !/usr/bin/env python3
import time
from libs.simulatedLedDriver import LedDriver

led = LedDriver()

led.lightRed()
time.sleep(1)
led.lightGreen()
time.sleep(1)
led.lightYellow()
